# Wyr: Wireguard Setup Tool

Wireguard VPN is elegantly simple, but configuring it for basic functionality takes too many steps.  Wyr is yet another tool to automate this process featuring:

* Can generate client config files.
* QR codes for quick mobile setup. (WIP)
* Single Rust binary.