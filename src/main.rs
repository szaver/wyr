#![warn(clippy::all)]
#![deny(clippy::correctness)]

mod wg;

use anyhow::{anyhow, Context, Error};
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(about = "Wireguard configuration helper")]
struct Args {
    #[structopt(long = "target", parse(from_os_str))]
    /// Local config file to edit with new peers (defaults to /etc/wireguard/wg0.conf)
    config_file: Option<PathBuf>,
    #[structopt(long = "ip")]
    /// Public IP of current server
    public_ip: Option<String>,
    #[structopt(subcommand)]
    cmd: Subcommand,
}

#[derive(Debug, StructOpt)]
enum Subcommand {
    /// Generate a client config file and optionally display a QR code.
    /// PersistentKeepAlive = 25 is included since many clients will be behind a NAT.
    ClientConf,
}
use Subcommand::*;
fn main() -> Result<(), Error> {
    let args = Args::from_args();

    let config_file: PathBuf = match args.config_file {
        None => {
            println!("config-file not specified, defaulting to /etc/wireguard/wg0.conf");
            "/etc/wireguard/wg0.conf".into()
        }
        Some(val) => val,
    };

    let pub_ip: String = match args.public_ip {
        None => {
            println!("public ip not specified, attempting to fetch it from http://www.myip.ch (not secure).");
            match my_internet_ip::get() {
                Ok(ip) => ip.to_string(),
                Err(e) => return Err(anyhow!("Error detecting public IP:\n{:?}", e)),
            }
        }
        Some(val) => val,
    };

    match args.cmd {
        ClientConf => {
            let mut new_config = wg::Config::new()?;
            let mut local_config =
                wg::Config::open(&config_file).context("opening local wireguard config file")?;

            // find the next available peer ip address
            let peer_ip = local_config.suggest_peer_ip()?;

            // populate the new config file (scoped for clarity)
            {
                // own ip address
                new_config.set_address(&peer_ip);

                // generating the new config file
                let peer = wg::PeerConfigBuilder::default()
                    .pubkey(local_config.pubkey()?)
                    .allowed_ips(vec![local_config.address()?])
                    .endpoint(format!("{}:{}", pub_ip, 51820))
                    .persistent_keep_alive(25)
                    .build()
                    .map_err(|e| anyhow!("{:?}", e))?;
                new_config.add_peer(peer)
            }

            // populate new peer section in local config file
            {
                let peer = wg::PeerConfigBuilder::default()
                    .pubkey(new_config.pubkey()?)
                    .allowed_ips(vec![format!("{}/{}", new_config.address()?.addr(), 32)
                        .parse()
                        .expect("never panics")])
                    .build()
                    .map_err(|e| anyhow!("{:?}", e))?;
                local_config.add_peer(peer);
            }

            // write the local config file
            local_config.save(&config_file)?;

            // print the new config file
            println!("{}", new_config.to_string());
        }
    }

    Ok(())
}
