mod cli;
mod config;

pub use config::Config;
pub use config::{PeerConfig, PeerConfigBuilder};

// use anyhow::{Context, Error};

// pub fn key_pair() -> Result<(String, String), Error> {
//     let private: String = cli::genkey().context("QR generation")?;
//     let public = cli::pubkey(&private)?;

//     Ok((public, private))
// }
