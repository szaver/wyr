use anyhow::{anyhow, Context, Error};
use std::io::Write;
use std::process::{Command, Stdio};

pub fn genkey() -> Result<String, Error> {
    let mut cmd = Command::new("wg");
    cmd.arg("genkey");

    ok_output(&mut cmd, None).context("Error generating a wireguard private key")
}

pub fn pubkey(privkey: &str) -> Result<String, Error> {
    let mut cmd = Command::new("wg");
    cmd.arg("pubkey");

    ok_output(&mut cmd, Some(privkey)).context("Error generating a wireguard public key")
}

fn ok_output(cmd: &mut Command, pipe_in: Option<&str>) -> Result<String, Error> {
    // configure piped io
    cmd.stdout(Stdio::piped());
    cmd.stderr(Stdio::piped());
    cmd.stdin(Stdio::piped());

    // spawn the child process
    let mut child = cmd.spawn().context("Error spawning command")?;

    // write to stdin (if needed)
    if let Some(input) = pipe_in {
        let stdin = child
            .stdin
            .as_mut()
            .context("Error opening command stdin")?;
        stdin
            .write_all(input.as_bytes())
            .context("Error writing to child command stdin")?;
    }

    // wait for child to finish
    let output = child
        .wait_with_output()
        .context("Error collecting child output")?;

    // check for bad exit code
    if !output.status.success() {
        return Err(anyhow!("Unsuccessful execution: {:?}", output));
    }

    // convert result to utf8 string
    let output = String::from_utf8(output.stdout).context("Converting output bytes to utf8")?;
    // trim whitespace
    let output = output.trim().to_string();

    Ok(output)
}
