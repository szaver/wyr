use anyhow::{anyhow, Context, Error};
use derive_builder::Builder;
use ini::ini::SectionEntry::*;
use ini::{ini::Properties, Ini};
use ipnet::IpNet;
use std::fmt;
use std::net::IpAddr;
use std::path::Path;

pub struct Config {
    doc: Ini,
}

impl Config {
    /// Generates a new private key and creates a new skeleton config file with it
    pub fn new() -> Result<Self, Error> {
        let mut new_self = Self { doc: Ini::new() };

        let privkey = super::cli::genkey()?;
        new_self.set_privkey(privkey);

        Ok(new_self)
    }

    /// Opens an existing config file
    pub fn open(path: &Path) -> Result<Self, Error> {
        let doc = Ini::load_from_file(path).context("loading wireguard config file")?;
        Ok(Self { doc })
    }

    /// Saves the config file to disk
    pub fn save(&self, path: &Path) -> Result<(), Error> {
        self.doc.write_to_file(path)?;
        Ok(())
    }

    /// Looks up a value in the [Interface] section
    fn iface_val(&self, key: &str) -> Result<&str, Error> {
        self.doc
            .section(Some("Interface"))
            .and_then(|section| section.get(key))
            .ok_or_else(|| anyhow!("Couldn't find key {} in [Interface] section.", key))
    }

    /// Overwrites a value in the [Interface] section
    fn set_iface_val(&mut self, key: &str, new_val: String) {
        let section = self
            .doc
            .entry(Some("Interface".to_string()))
            .or_insert(Properties::new());
        section.insert(key, new_val);
    }

    /// Returns a reference to the value of PrivateKey in the [Interface] section
    pub fn privkey(&self) -> Result<&str, Error> {
        self.iface_val("PrivateKey")
    }

    /// Sets the PrivateKey
    fn set_privkey(&mut self, key: String) {
        self.set_iface_val("PrivateKey", key)
    }

    /// Returns the parsed value of Address in the [Interface] section
    /// This refers to the ip address and subnet mask of the WireGuard network interface (i.e. wg0)
    pub fn address(&self) -> Result<IpNet, Error> {
        let address = self.iface_val("Address")?.parse()?;
        Ok(address)
    }

    pub fn set_address(&mut self, val: &IpNet) {
        self.set_iface_val("Address", val.to_string());
    }

    // /// Returns a reference to the value of Address in the [Interface] section
    // /// This refers to the ip address and subnet mask of the WireGuard network interface (i.e. wg0)
    // pub fn listen_port(&self) -> Result<u16, Error> {
    //     let port = self.iface_val("ListenPort")?.parse()?;
    //     Ok(port)
    // }

    pub fn pubkey(&self) -> Result<String, Error> {
        let privkey = self.privkey()?;
        let pubkey = super::cli::pubkey(privkey)?;

        Ok(pubkey)
    }

    // /// Returns the private key as defined in the config file,
    // /// and the public key computed with the `wg pubkey` command.
    // pub fn key_pair(&self) -> Result<(String, String), Error> {
    //     let privkey = self.privkey()?;
    //     let pubkey = self.pubkey()?;

    //     Ok((pubkey, privkey.to_string()))
    // }

    /// Adds a peer to the config file
    pub fn add_peer(&mut self, peer: PeerConfig) {
        let allowed_ips = peer
            .allowed_ips
            .iter()
            .map(|ip_net| ip_net.to_string())
            .collect::<Vec<_>>()
            .join(",");

        let mut properties = Properties::new();

        // set required fields
        properties.insert("PublicKey", peer.pubkey.clone());
        properties.insert("AllowedIps", allowed_ips);

        // set optional fields
        if let Some(v) = peer.endpoint.clone() {
            properties.insert("Endpoint", v);
        }
        if let Some(v) = peer.persistent_keep_alive {
            properties.insert("PersistentKeepAlive", v.to_string());
        }

        match self.doc.entry(Some("Peer".into())) {
            Vacant(vacant_entry) => {
                vacant_entry.insert(properties);
            }
            Occupied(mut occupied_entry) => {
                occupied_entry.append(properties);
            }
        };
    }

    /// returns true if the IP is already used by a peer or the server itself
    pub fn ip_in_use(&self, ip: &IpAddr) -> Result<bool, Error> {
        // check self address
        if let Ok(address) = self.address() {
            if address.addr() == *ip {
                return Ok(true);
            }
        }

        // check addresses of existing peers
        for peer in self.doc.section_all(Some("Peer")) {
            match peer.get("AllowedIps") {
                None => {
                    continue;
                }
                Some(allowed_ips) => {
                    for ip_net in allowed_ips.split(',') {
                        let ip_net: IpNet = ip_net.parse()?;
                        if ip_net.contains(ip) {
                            return Ok(true);
                        }
                    }
                }
            }
        }

        // address must be free
        Ok(false)
    }

    /// Returns the next free ip address
    pub fn suggest_peer_ip(&self) -> Result<IpNet, Error> {
        let net = self.address()?;

        for addr in net.hosts() {
            if !self.ip_in_use(&addr)? {
                return Ok(format!("{}/{}", addr, net.prefix_len())
                    .parse()
                    .expect("never panics"));
            }
        }

        Err(anyhow!("Couldn't find any free ip addresses!"))
    }
}

impl fmt::Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut buf = Vec::new();
        self.doc
            .write_to(&mut buf)
            .expect("Error converting ini to string");
        let buf = String::from_utf8(buf).expect("Never panics");
        write!(f, "{}", buf)
    }
}

#[derive(Debug, Clone, Builder)]
pub struct PeerConfig {
    pubkey: String,
    allowed_ips: Vec<IpNet>,
    #[builder(default, setter(strip_option))]
    endpoint: Option<String>,
    #[builder(default, setter(strip_option))]
    persistent_keep_alive: Option<u32>,
}
